-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        8.0.33 - MySQL Community Server - GPL
-- 服务器操作系统:                      Win64
-- HeidiSQL 版本:                  12.5.0.6677
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- 导出  表 boot-local.file 结构
DROP TABLE IF EXISTS `file`;
CREATE TABLE IF NOT EXISTS `file` (
  `file_id` bigint unsigned NOT NULL COMMENT '文件id',
  `file_name` varchar(255) NOT NULL DEFAULT '' COMMENT '文件名',
  `original_file_name` varchar(255) NOT NULL COMMENT '原始文件名',
  `file_type` varchar(255) NOT NULL DEFAULT '' COMMENT '文件类型/后缀',
  `file_path` varchar(255) NOT NULL DEFAULT '' COMMENT '文件存储目录全路径',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '文件url',
  `file_size` bigint unsigned DEFAULT NULL COMMENT '文件大小',
  `create_user_id` bigint unsigned NOT NULL COMMENT '创建人id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='文件上传表';

-- 正在导出表  boot-local.file 的数据：~11 rows (大约)
DELETE FROM `file`;
INSERT INTO `file` (`file_id`, `file_name`, `original_file_name`, `file_type`, `file_path`, `url`, `file_size`, `create_user_id`, `create_time`) VALUES
	(1778078676452737026, 'bg.jpg', '', 'jpg', 'D:\\upload\\2024-04-10\\15cca22dc9644742a86bf72698ded611.jpg', 'http://localhost:8080/api/file/1778078676452737026', 26174, 1773935177843187713, '2024-04-10 23:12:39'),
	(1778079435944722434, 'bg.jpg', '', 'jpg', 'D:\\upload\\2024-04-10\\cfe964867aaa441d91ed58060d7345b4.jpg', 'http:/localhost:8080/api/file/1778079435944722434', 26174, 1773935177843187713, '2024-04-10 23:15:46'),
	(1778688603525304321, 'bg.jpg', '', 'jpg', 'D:\\upload\\2024-04-12\\16df337723f74ef9b630902e053e70f0.jpg', 'http:/localhost:8080/api/file/1778688603525304321', 26174, 1773935177843187713, '2024-04-12 15:36:24'),
	(1778688653651431425, 'bg.jpg', '', 'jpg', 'D:\\upload\\2024-04-12\\d636ba71b8e646c39fcdab72c4a5ed2d.jpg', 'http:/localhost:8080/api/file/1778688653651431425', 26174, 1773935177843187713, '2024-04-12 15:36:36'),
	(1778690383105576961, 'bg.jpg', '', 'jpg', 'D:\\upload\\2024-04-12\\edc40aa18f7741bab3b6b6f77809120f.jpg', 'http:/localhost:8080/api/file/1778690383105576961', 26174, 1773935177843187713, '2024-04-12 15:43:28'),
	(1778690456644308993, 'bg.jpg', '', 'jpg', 'D:\\upload\\2024-04-12\\c72831cff00e465fb6504e8b0f2d38a3.jpg', 'http:/localhost:8080/api/file/1778690456644308993', 26174, 1773935177843187713, '2024-04-12 15:43:46'),
	(1778690653277474818, 'bg.jpg', '', 'jpg', 'D:\\upload\\2024-04-12\\c2d6a24aa44a4708afda01b02cdc468f.jpg', 'http:/localhost:8080/api/file/1778690653277474818', 26174, 1773935177843187713, '2024-04-12 15:44:33'),
	(1779116966735859713, '800a5c4f9a194d52956d7a8684eb4476.jpg', 'bg.jpg', 'jpg', 'D:\\upload\\2024-04-13\\800a5c4f9a194d52956d7a8684eb4476.jpg', 'http:/localhost:8080/api/file/1779116966735859713', 26174, 1773935177843187713, '2024-04-13 19:58:32'),
	(1779118425456066561, 'a0270d95baf14a56927b265342641d3a.jpg', 'bg.jpg', 'jpg', 'D:\\upload\\2024-04-13\\a0270d95baf14a56927b265342641d3a.jpg', 'http:/localhost:8080/api/file/1779118425456066561', 26174, 1773935177843187713, '2024-04-13 20:04:22'),
	(1779121122582282241, '4a3966e6e36449a0854b54f3a5c32ce9.png', 'logo.png', 'png', 'D:\\upload\\2024-04-13\\4a3966e6e36449a0854b54f3a5c32ce9.png', 'http:/localhost:8080/api/file/1779121122582282241', 76190, 1773935177843187713, '2024-04-13 20:15:05'),
	(1779121167377448961, '4d390c19d61848bc8a7374eb04ea4c08.png', 'logo.png', 'png', 'D:\\upload\\2024-04-13\\4d390c19d61848bc8a7374eb04ea4c08.png', 'http:/localhost:8080/api/file/1779121167377448961', 76190, 1773935177843187713, '2024-04-13 20:15:15');

-- 导出  表 boot-local.menu_info 结构
DROP TABLE IF EXISTS `menu_info`;
CREATE TABLE IF NOT EXISTS `menu_info` (
  `authority_id` bigint unsigned NOT NULL COMMENT '主键id',
  `authority_name` varchar(255) NOT NULL COMMENT '菜单名称',
  `order_number` int DEFAULT (0) COMMENT '排序顺序',
  `menu_url` varchar(255) DEFAULT NULL COMMENT '菜单链接',
  `menu_url_target` varchar(50) NOT NULL DEFAULT '_self' COMMENT '打开链接的方式(_self,_blank)，默认_self',
  `menu_icon` varchar(255) DEFAULT NULL COMMENT '菜单图标',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `authority` varchar(255) DEFAULT NULL COMMENT '权限标识',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `menu_type` int NOT NULL DEFAULT '0' COMMENT '菜单类型1for按钮0for菜单',
  `parent_id` bigint NOT NULL COMMENT '父节点id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='菜单表';

-- 正在导出表  boot-local.menu_info 的数据：~35 rows (大约)
DELETE FROM `menu_info`;
INSERT INTO `menu_info` (`authority_id`, `authority_name`, `order_number`, `menu_url`, `menu_url_target`, `menu_icon`, `create_time`, `authority`, `update_time`, `menu_type`, `parent_id`) VALUES
	(1, '常规管理', 1, NULL, '_self', 'fa fa-address-book', '2024-04-06 20:16:35', NULL, '2024-05-02 16:16:05', 0, -1),
	(2, '主页模板', 2, NULL, '_self', 'fa fa-home', '2024-04-06 20:22:19', NULL, '2024-05-01 18:24:30', 0, 1),
	(3, '主页一', 3, 'page/welcome-1.html', '_self', 'fa fa-tachometer', '2024-04-06 20:24:04', NULL, '2024-04-06 20:24:12', 0, 2),
	(4, '主页二', 4, 'page/welcome-2.html', '_self', 'fa fa-tachometer', '2024-04-06 20:25:43', NULL, '2024-04-06 20:25:47', 0, 2),
	(5, '主页三', 5, 'page/welcome-3.html', '_self', 'fa fa-tachometer', '2024-04-06 20:26:36', NULL, '2024-04-06 20:26:39', 0, 2),
	(6, '菜单管理', 6, 'page/menu.html', '_self', 'fa fa-window-maximize', '2024-04-06 20:33:48', NULL, '2024-04-06 20:33:52', 0, 1),
	(7, '系统设置', 7, 'page/setting.html', '_self', 'fa fa-gears', '2024-04-06 20:35:04', NULL, '2024-04-06 20:35:08', 0, 1),
	(8, '用户管理', 8, 'page/user.html', '_self', 'fa fa fa-user', '2024-04-06 20:35:42', NULL, '2024-05-02 18:04:03', 0, 1),
	(9, '表单示例', 9, NULL, '_self', 'fa fa-calendar', '2024-04-06 20:36:24', NULL, '2024-04-06 20:36:28', 0, 1),
	(10, '普通表单', 10, 'page/form.html', '_self', 'fa fa-list-alt', '2024-04-06 20:37:17', NULL, '2024-04-06 20:37:20', 0, 9),
	(11, '分步表单', 11, 'page/form-step.html', '_self', 'fa fa-navicon', '2024-04-06 20:38:10', NULL, '2024-04-06 20:38:12', 0, 9),
	(12, '登录模板', 12, NULL, '_self', 'fa fa-flag-o', '2024-04-06 20:39:00', NULL, '2024-04-06 20:39:04', 0, 1),
	(13, '登录-1', 13, 'page/login-1.html', '_blank', 'fa fa-stumbleupon-circle', '2024-04-06 20:40:14', NULL, '2024-04-06 20:40:18', 0, 12),
	(14, '登录-2', 14, 'page/login-2.html', '_blank', 'fa fa-viacoin', '2024-04-06 20:41:28', NULL, '2024-04-06 20:41:31', 0, 12),
	(15, '登录-3', 15, 'page/login-3.html', '_blank', 'fa fa-tags', '2024-04-06 20:42:20', NULL, '2024-04-06 20:42:27', 0, 12),
	(16, '异常页面', 16, NULL, '_self', 'fa fa-home', '2024-04-06 20:43:27', NULL, '2024-04-06 20:43:31', 0, 1),
	(17, '404页面', 17, 'page/404.html', '_self', 'fa fa-hourglass-end', '2024-04-06 20:44:25', NULL, '2024-04-06 20:44:28', 0, 16),
	(18, '其它界面', 18, NULL, '_self', 'fa fa-snowflake-o', '2024-04-06 20:45:14', NULL, '2024-04-06 20:45:17', 0, 1),
	(19, '按钮示例', 19, 'page/button.html', '_self', 'fa fa-snowflake-o', '2024-04-06 20:46:18', NULL, '2024-04-06 20:46:21', 0, 18),
	(20, '弹出层', 20, 'page/layer.html', '_self', 'fa fa-shield', '2024-04-06 20:47:11', NULL, '2024-04-06 20:47:15', 0, 18),
	(21, '组件管理', 21, NULL, '_self', 'fa fa-lemon-o', '2024-04-06 20:48:49', NULL, '2024-04-06 20:48:51', 0, -1),
	(22, '图标列表', 22, 'page/icon.html', '_self', 'fa fa-dot-circle-o', '2024-04-06 20:50:07', NULL, '2024-04-06 20:50:10', 0, 21),
	(23, '图标选择', 23, 'page/icon-picker.html', '_self', 'fa fa-adn', '2024-04-06 20:50:52', NULL, '2024-04-06 20:50:57', 0, 21),
	(24, '颜色选择', 24, 'page/color-select.html', '_self', 'fa fa-dashboard', '2024-04-06 20:51:38', NULL, '2024-04-06 20:51:41', 0, 21),
	(25, '下拉选择', 25, 'page/table-select.html', '_self', 'fa fa-angle-double-down', '2024-04-06 20:52:24', NULL, '2024-04-06 20:52:27', 0, 21),
	(26, '文件上传', 26, 'page/upload.html', '_self', 'fa fa-arrow-up', '2024-04-06 20:53:01', NULL, '2024-04-06 20:53:04', 0, 21),
	(27, '富文本编辑器', 27, 'page/editor.html', '_self', 'fa fa-edit', '2024-04-06 20:53:49', NULL, '2024-04-06 20:53:53', 0, 21),
	(28, '省市县区选择器', 28, 'page/area.html', '_self', 'fa fa-rocket', '2024-04-06 20:54:39', NULL, '2024-04-06 20:54:43', 0, 21),
	(29, '其它管理', 29, NULL, '_self', 'fa fa-slideshare', '2024-04-06 20:55:16', NULL, '2024-04-06 20:55:19', 0, -1),
	(30, '多级菜单', 30, NULL, '_self', 'fa fa-meetup', '2024-04-06 20:56:25', NULL, '2024-04-06 20:56:29', 0, 29),
	(31, '按钮1', 31, 'page/button.html?v=1', '_self', 'fa fa-calendar', '2024-04-06 20:57:16', NULL, '2024-04-06 20:57:20', 0, 30),
	(32, '按钮2', 32, 'page/button.html?v=2', '_self', 'fa fa-snowflake-o', '2024-04-06 20:58:21', NULL, '2024-04-06 20:58:24', 0, 31),
	(33, '按钮3', 33, 'page/button.html?v=3', '_self', 'fa fa-snowflake-o', '2024-04-06 21:00:00', NULL, '2024-04-06 21:00:03', 0, 32),
	(34, '表单4', 34, 'page/form.html?v=1', '_self', 'fa fa-calendar', '2024-04-06 21:01:07', NULL, '2024-04-06 21:01:10', 0, 32),
	(35, '失效菜单', 35, 'page/error.html', '_self', 'fa fa-superpowers', '2024-04-06 21:02:08', NULL, '2024-04-06 21:02:12', 0, 29);

-- 导出  表 boot-local.opr_log 结构
DROP TABLE IF EXISTS `opr_log`;
CREATE TABLE IF NOT EXISTS `opr_log` (
  `id` bigint unsigned NOT NULL COMMENT '主键ID',
  `opr_content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '操作内容',
  `opr_api` varchar(255) NOT NULL COMMENT '操作的api接口',
  `opr_api_params` longtext COMMENT '操作的api接口参数',
  `opr_id` bigint unsigned NOT NULL COMMENT '操作人ID',
  `opr_username` varchar(255) NOT NULL COMMENT '操作人账号',
  `opr_ip` varchar(50) DEFAULT NULL COMMENT '操作人ip',
  `stacktrace` longtext COMMENT '异常堆栈',
  `resp_json` longtext COMMENT '返回对象的json字符串',
  `creatTime` datetime NOT NULL COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='操作日志表';

-- 正在导出表  boot-local.opr_log 的数据：~0 rows (大约)
DELETE FROM `opr_log`;

-- 导出  表 boot-local.site_info 结构
DROP TABLE IF EXISTS `site_info`;
CREATE TABLE IF NOT EXISTS `site_info` (
  `id` int unsigned NOT NULL DEFAULT (0) COMMENT '主键id，设置为0',
  `home_page_title` varchar(255) NOT NULL COMMENT '主页title',
  `home_page_href` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主页href',
  `logo_title` varchar(255) NOT NULL COMMENT '图标title',
  `logo_image` varchar(255) NOT NULL COMMENT '图标',
  `logo_href` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '图标链接',
  `sitename` varchar(255) NOT NULL COMMENT '网站名称',
  `domain` varchar(255) NOT NULL COMMENT '网站域名',
  `cache_time` int unsigned DEFAULT (0) COMMENT '缓存时间,单位分钟',
  `max_file_upload` int unsigned DEFAULT (0) COMMENT '最大文件上传,单位kb',
  `file_upload_ext` varchar(255) DEFAULT NULL COMMENT '上传文件类型 , 分隔',
  `meta_keywords` varchar(255) DEFAULT NULL COMMENT 'META关键词',
  `meta_description` varchar(1000) DEFAULT NULL COMMENT 'META描述',
  `copyright` varchar(1000) DEFAULT NULL COMMENT '版权信息'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='网站信息';

-- 正在导出表  boot-local.site_info 的数据：~1 rows (大约)
DELETE FROM `site_info`;
INSERT INTO `site_info` (`id`, `home_page_title`, `home_page_href`, `logo_title`, `logo_image`, `logo_href`, `sitename`, `domain`, `cache_time`, `max_file_upload`, `file_upload_ext`, `meta_keywords`, `meta_description`, `copyright`) VALUES
	(0, '首页', 'page/welcome-1.html?t=1', 'LAYUI MINI', '/4d390c19d61848bc8a7374eb04ea4c08.png', NULL, 'layuimini', 'http://localhost:8080', 0, 2048, 'png|gif|jpg|jpeg|zip|rar', '', 'layuimini，最简洁、清爽、易用的layui后台框架模板。', '© 2019 layuimini.99php.cn MIT license');

-- 导出  表 boot-local.user 结构
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` bigint unsigned NOT NULL COMMENT 'id',
  `username` varchar(255) NOT NULL DEFAULT '' COMMENT '用户名',
  `password` varchar(255) NOT NULL DEFAULT '' COMMENT '密码',
  `last_login_time` datetime DEFAULT NULL COMMENT '上次登录时间',
  `salt` varchar(255) NOT NULL COMMENT '密码md5加密盐',
  `next_change_password_time` datetime NOT NULL COMMENT '下次修改密码时间',
  `gender` tinyint unsigned NOT NULL DEFAULT '1' COMMENT '性别// 1 for male 0 for female',
  `mobile` varchar(50) DEFAULT '1' COMMENT '手机',
  `email` varchar(50) DEFAULT '1' COMMENT '邮箱',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `status` int NOT NULL DEFAULT '1' COMMENT '状态，正常1，禁用0',
  UNIQUE KEY `username_uni_key` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='用户表';

-- 正在导出表  boot-local.user 的数据：~1 rows (大约)
DELETE FROM `user`;
INSERT INTO `user` (`id`, `username`, `password`, `last_login_time`, `salt`, `next_change_password_time`, `gender`, `mobile`, `email`, `remark`, `status`) VALUES
	(1773935177843187713, 'admin', 'd18698aa73c82e75e6a603fc656de910', '2024-05-03 12:17:08', 'AgZnF_a2te951HqFcywNc', '2024-04-29 12:47:59', 1, '13823684568', '826935261@qq.com', '可惜不是你，陪我到最后。', 1);

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
