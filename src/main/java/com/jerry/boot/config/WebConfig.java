package com.jerry.boot.config;

import com.jerry.boot.core.web.JBootFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.DelegatingFilterProxy;

@Configuration
public class WebConfig {

    @Bean
    public FilterRegistrationBean<DelegatingFilterProxy> delegatingFilterProxy() {
        FilterRegistrationBean<DelegatingFilterProxy> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new DelegatingFilterProxy());
        // 拦截所有访问
        registrationBean.addUrlPatterns("/*");
        //被代理filter
        registrationBean.addInitParameter("targetBeanName", "jBootFilter");
        //指明作用于filter的所有生命周期
        registrationBean.addInitParameter("targetFilterLifecycle", "true");
        registrationBean.setName("DelegatingFilterProxy");
        //设置优先级
        registrationBean.setOrder(Integer.MIN_VALUE + 1);
        registrationBean.setEnabled(false);
        return registrationBean;
    }

    @Bean(name = "jBootFilter")
    public JBootFilter jBootFilter() {
        return new JBootFilter();
    }


}
