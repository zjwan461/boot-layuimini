package com.jerry.boot.business.user;

import com.jerry.boot.business.user.pojo.UserInfo;
import com.jerry.boot.business.user.pojo.UserPageReq;
import com.jerry.boot.core.base.validation.Select;
import com.jerry.boot.core.base.validation.Update;
import com.jerry.boot.core.mvc.JsonResult;
import com.jerry.boot.core.mvc.NativeResponse;
import com.jerry.boot.core.mvc.PageBase;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Resource
    private UserService userService;

    @GetMapping("/list")
    @NativeResponse
    public PageBase<UserInfo> list(@Validated(Select.class) UserPageReq pageReq) {
        return userService.queryPage(pageReq);
    }

    @PostMapping("/update")
    public JsonResult update(@RequestBody @Validated(Update.class) UserInfo userInfo) {
        return userService.update(userInfo);
    }

}
