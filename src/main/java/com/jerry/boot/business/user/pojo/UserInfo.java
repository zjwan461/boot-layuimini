package com.jerry.boot.business.user.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.jerry.boot.core.base.validation.Update;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserInfo {

    @NotNull(groups = {Update.class})
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lastLoginTime;
    private String username;
    private Integer gender; // 1 for male 0 for female
    private String mobile;
    private String email;
    private String remark;
    private Integer status = 1; //状态，1for正常，0for禁用，默认1正常
}
