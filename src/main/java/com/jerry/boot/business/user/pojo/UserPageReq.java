package com.jerry.boot.business.user.pojo;

import com.jerry.boot.core.mvc.PageReq;
import lombok.Data;

@Data
public class UserPageReq extends PageReq {

    private String username;

    private Integer gender;

    private String mobile;

    private String email;
}
