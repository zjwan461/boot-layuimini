package com.jerry.boot.business.user;

import com.jerry.boot.business.user.pojo.UserInfo;
import com.jerry.boot.business.user.pojo.UserPageReq;
import com.jerry.boot.core.mvc.JsonResult;
import com.jerry.boot.core.mvc.PageBase;

public interface UserService {

    User getUserByUsername(String username);

    void updateUserLastLoginTime(String username);

    PageBase<UserInfo> queryPage(UserPageReq userPageReq);

    JsonResult update(UserInfo userInfo);
}
