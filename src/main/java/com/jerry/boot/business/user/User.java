package com.jerry.boot.business.user;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("user")
public class User implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    private String username;

    private String password;

    private Date lastLoginTime;

    private String salt;

    private Date nextChangePasswordTime;

    private Integer gender; // 1 for male 0 for female

    private String mobile;

    private String email;

    private String remark;

    private Integer status;

}
