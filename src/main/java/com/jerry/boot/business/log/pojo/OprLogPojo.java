package com.jerry.boot.business.log.pojo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import java.util.Date;

@Data
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class OprLogPojo {

    private Long id;

    private String oprContent;

    private String oprApi;

    private String oprApiParams;

    private Long oprId;

    private String oprUsername;

    private String oprIp;

    private Date createTime;

    private String stacktrace;

    private String respJson;
}
