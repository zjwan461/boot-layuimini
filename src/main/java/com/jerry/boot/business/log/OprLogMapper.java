package com.jerry.boot.business.log;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OprLogMapper extends BaseMapper<OprLog> {
}
