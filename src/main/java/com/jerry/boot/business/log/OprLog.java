package com.jerry.boot.business.log;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName("opr_log")
public class OprLog {

    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    private String oprContent;

    private String oprApi;

    private String oprApiParams;

    private Long oprId;

    private String oprUsername;

    private String oprIp;

    private Date createTime;

    private String stacktrace;

    private String respJson;
}
