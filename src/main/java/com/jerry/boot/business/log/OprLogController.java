package com.jerry.boot.business.log;

import com.jerry.boot.business.log.pojo.OprLogPojo;
import com.jerry.boot.core.base.validation.Select;
import com.jerry.boot.core.mvc.NativeResponse;
import com.jerry.boot.core.mvc.PageBase;
import com.jerry.boot.core.mvc.PageReq;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/api/log")
public class OprLogController {

    @Resource
    private OprLogService oprLogService;

    @GetMapping("/list")
    @NativeResponse
    public PageBase<OprLogPojo> list(@Validated(Select.class) PageReq pageReq) {
        return oprLogService.listPage(pageReq);
    }
}
