package com.jerry.boot.business.log;

import com.jerry.boot.business.log.pojo.OprLogPojo;
import com.jerry.boot.core.mvc.PageBase;
import com.jerry.boot.core.mvc.PageReq;

public interface OprLogService {

    void writeLog(OprLogPojo oprLogPojo);

    PageBase<OprLogPojo> listPage(PageReq pageReq);
}
