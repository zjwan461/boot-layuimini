package com.jerry.boot.business.log;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Log {

    String value();

    boolean sync() default true;

    int[] argIndexes();

    String argExpression();

    boolean saveResp() default false;
}
