package com.jerry.boot.business.menu;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @author jerry
 * @date 2024/4/6 下午8:07
 */
@TableName("menu_info")
@Data
public class MenuInfo {


    /**
     * authorityId : 1
     * authorityName : 系统管理
     * orderNumber : 1
     * menuUrl : null
     * menuIcon : layui-icon-set
     * createTime : 2018/06/29 11:05:41
     * authority : null
     * checked : 0
     * updateTime : 2018/07/13 09:13:42
     * isMenu : 0
     * parentId : -1
     */

    @TableId(type = IdType.ASSIGN_ID)
    private Long authorityId;
    private String authorityName;
    private int orderNumber;
    private String menuUrl;
    private String menuUrlTarget;
    private String menuIcon;
    private Date createTime;
    private String authority;
    @TableField(exist = false)
    private int checked;
    private Date updateTime;
    private int menuType;
    private Long parentId;


}
