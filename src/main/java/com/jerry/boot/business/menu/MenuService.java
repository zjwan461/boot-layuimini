package com.jerry.boot.business.menu;

import com.jerry.boot.business.base.pojo.MenuInfo;
import com.jerry.boot.business.menu.pojo.Menu;

import java.util.List;

/**
 * @author jerry
 * @date 2024/4/6 下午8:12
 */
public interface MenuService {

    List<MenuInfo> listMenuTree();

    List<Menu> listMenu();

    void updateMenu(Menu menu);
}
