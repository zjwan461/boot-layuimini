package com.jerry.boot.business.menu;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jerry.boot.business.menu.pojo.Menu;
import com.jerry.boot.core.base.ApiException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author jerry
 * @date 2024/4/6 下午8:12
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuInfoMapper, MenuInfo> implements MenuService {
    @Override
    public List<com.jerry.boot.business.base.pojo.MenuInfo> listMenuTree() {
        List<MenuInfo> list = this.list(Wrappers.lambdaQuery(MenuInfo.class)
                .eq((SFunction<MenuInfo, Integer>) MenuInfo::getMenuType, 0)
                .orderByAsc((SFunction<MenuInfo, Integer>) MenuInfo::getOrderNumber)
        );
        List<com.jerry.boot.business.base.pojo.MenuInfo> result = new ArrayList<>();
        for (MenuInfo menuInfo : list) {
            if (menuInfo.getParentId() == -1) {
                com.jerry.boot.business.base.pojo.MenuInfo info = new com.jerry.boot.business.base.pojo.MenuInfo();
                info.setHref(menuInfo.getMenuUrl());
                info.setIcon(menuInfo.getMenuIcon());
                info.setTarget(menuInfo.getMenuUrlTarget());
                info.setTitle(menuInfo.getAuthorityName());
                info.setChild(listChildren(list, menuInfo.getAuthorityId()));
                result.add(info);
            }
        }
        return result;
    }

    private List<com.jerry.boot.business.base.pojo.MenuInfo> listChildren(List<MenuInfo> list, Long parentId) {
        List<com.jerry.boot.business.base.pojo.MenuInfo> result = new ArrayList<>();
        for (MenuInfo item : list) {
            if (item.getParentId().longValue() == parentId) {
                com.jerry.boot.business.base.pojo.MenuInfo info = new com.jerry.boot.business.base.pojo.MenuInfo();
                info.setHref(item.getMenuUrl());
                info.setIcon(item.getMenuIcon());
                info.setTarget(item.getMenuUrlTarget());
                info.setTitle(item.getAuthorityName());
                info.setChild(listChildren(list, item.getAuthorityId()));
                result.add(info);
            }
        }
        return result;
    }

    @Override
    public List<Menu> listMenu() {
        return this.list().stream().map(x -> new Menu().map(x)).collect(Collectors.toList());
    }

    @Transactional(rollbackFor = Throwable.class)
    @Override
    public void updateMenu(Menu menu) {
        MenuInfo menuInfo = this.getById(menu.getAuthorityId());
        if (menuInfo == null) {
            throw new ApiException(106, menu.getAuthorityId() + "");
        }
        menuInfo.setAuthorityName(StrUtil.isNotBlank(menu.getAuthorityName()) ? menu.getAuthorityName() : null);
        menuInfo.setOrderNumber(menu.getOrderNumber());
        menuInfo.setMenuUrl(StrUtil.isNotBlank(menu.getMenuUrl()) ? menu.getMenuUrl() : null);
        menuInfo.setMenuIcon(StrUtil.isNotBlank(menu.getMenuIcon()) ? menu.getMenuIcon() : null);
        menuInfo.setAuthority(StrUtil.isNotBlank(menu.getAuthority()) ? menu.getAuthority() : null);
        menuInfo.setUpdateTime(menu.getUpdateTime());
        menuInfo.setMenuType(menu.getIsMenu());
        menuInfo.setParentId(menu.getParentId());
        this.updateById(menuInfo);
    }
}
