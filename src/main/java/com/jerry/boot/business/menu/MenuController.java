package com.jerry.boot.business.menu;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.jerry.boot.business.menu.pojo.Menu;
import com.jerry.boot.core.base.validation.Update;
import com.jerry.boot.core.mvc.JsonResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/api/menu")
public class MenuController {

    @Resource
    private MenuService menuService;

    @GetMapping("/list")
    public List<Menu> listMenu() {
        return menuService.listMenu();
    }

    @PostMapping("/update")
    public JsonResult updateMenu(@RequestBody @Validated(Update.class) Menu menu) {
        String menuIcon = menu.getMenuIcon();
        //补充上fa前缀的css class,否则影响菜单显示
        if (StrUtil.isNotBlank(menuIcon) && !StrUtil.startWith("fa ", menuIcon)) {
            menuIcon = "fa " + menuIcon;
            menu.setMenuIcon(menuIcon);
        }
        //设置更新时间为当天
        menu.setUpdateTime(DateUtil.date());
        menuService.updateMenu(menu);
        return JsonResult.ok();
    }

}
