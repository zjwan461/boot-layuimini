package com.jerry.boot.business.menu;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author jerry
 * @date 2024/4/6 下午8:11
 */
@Mapper
public interface MenuInfoMapper extends BaseMapper<MenuInfo> {
}
