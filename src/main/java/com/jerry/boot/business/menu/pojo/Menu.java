package com.jerry.boot.business.menu.pojo;

import com.jerry.boot.business.menu.MenuInfo;
import com.jerry.boot.core.base.validation.Update;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Data
public class Menu implements Serializable {

    /**
     * authorityId : 1
     * authorityName : 系统管理
     * orderNumber : 1
     * menuUrl : null
     * menuIcon : layui-icon-set
     * createTime : 2018/06/29 11:05:41
     * authority : null
     * checked : 0
     * updateTime : 2018/07/13 09:13:42
     * isMenu : 0
     * parentId : -1
     */

    @NotNull(groups = {Update.class})
    private Long authorityId;
    @NotBlank(groups = {Update.class})
    private String authorityName;
    @NotNull(groups = {Update.class})
    private int orderNumber;
    private String menuUrl;
    private String menuIcon;
    private Date createTime;
    private String authority;
    private int checked;
    private Date updateTime;
    private int isMenu;
    private Long parentId;

    public Menu map(MenuInfo menuInfo) {
        this.setAuthorityId(menuInfo.getAuthorityId());
        this.setAuthorityName(menuInfo.getAuthorityName());
        this.setOrderNumber(menuInfo.getOrderNumber());
        this.setMenuUrl(menuInfo.getMenuUrl());
        this.setMenuIcon(menuInfo.getMenuIcon());
        this.setCreateTime(menuInfo.getCreateTime());
        this.setAuthority(menuInfo.getAuthority());
        this.setUpdateTime(menuInfo.getUpdateTime());
        this.setIsMenu(menuInfo.getMenuType());
        this.setParentId(menuInfo.getParentId());
        return this;
    }
}
