package com.jerry.boot.business.base;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jerry.boot.business.base.pojo.SiteInfoDto;
import org.springframework.stereotype.Service;

@Service
public class SiteInfoServiceImpl extends ServiceImpl<SiteInfoMapper, SiteInfo> implements SiteInfoService {

    public static final int DEFAULT_SITE_ID = 0;

    @Override
    public SiteInfo getSiteInfo() {
        return this.getById(DEFAULT_SITE_ID);
    }

    @Override
    public void updateSiteinfo(SiteInfoDto siteInfoDto) {
        SiteInfo siteInfo = this.getSiteInfo();
        siteInfo.setDomain(siteInfoDto.getDomain());
        siteInfo.setCopyright(siteInfoDto.getCopyright());
        siteInfo.setCacheTime(siteInfoDto.getCacheTime());
        siteInfo.setHomePageHref(siteInfoDto.getHomePageHref());
        siteInfo.setHomePageTitle(siteInfoDto.getHomePageTitle());
        siteInfo.setLogoImage(siteInfoDto.getLogoImage());
        siteInfo.setLogoHref(siteInfoDto.getLogoHref());
        siteInfo.setLogoTitle(siteInfoDto.getLogoTitle());
        siteInfo.setMaxFileUpload(siteInfoDto.getMaxFileUpload());
        siteInfo.setFileUploadExt(siteInfoDto.getFileUploadExt());
        siteInfo.setMetaDescription(siteInfoDto.getMetaDescription());
        siteInfo.setMetaKeywords(siteInfoDto.getMetaKeywords());
        siteInfo.setSitename(siteInfoDto.getSitename());
        this.updateById(siteInfo);
    }
}
