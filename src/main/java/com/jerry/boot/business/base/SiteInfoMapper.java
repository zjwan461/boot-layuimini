package com.jerry.boot.business.base;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SiteInfoMapper extends BaseMapper<SiteInfo> {
}
