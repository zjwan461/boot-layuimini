package com.jerry.boot.business.base.pojo;

public class HomeInfo {

    /**
     * title : 首页
     * href : page/welcome-1.html?t=1
     */

    private String title;
    private String href;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }
}
