package com.jerry.boot.business.base.pojo;

import cn.hutool.core.bean.BeanUtil;
import com.jerry.boot.business.base.SiteInfo;
import com.jerry.boot.core.base.validation.Update;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
public class SiteInfoDto implements Serializable {

    @NotBlank(groups = Update.class)
    private String homePageTitle;

    @NotBlank(groups = Update.class)
    private String homePageHref;

    @NotBlank(groups = Update.class)
    private String logoTitle;

    private String logoImage;

    private String logoHref;

    @NotBlank(groups = Update.class)
    private String sitename;

    @NotBlank(groups = Update.class)
    private String domain;

    private int cacheTime;

    private int maxFileUpload;

    private String fileUploadExt;

    private String metaKeywords;

    private String metaDescription;

    @NotBlank(groups = Update.class)
    private String copyright;

    public static SiteInfoDto map(SiteInfo siteInfo) {
        SiteInfoDto siteInfoDto = new SiteInfoDto();
        BeanUtil.copyProperties(siteInfo, siteInfoDto);
        return siteInfoDto;
    }
}
