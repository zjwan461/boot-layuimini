package com.jerry.boot.business.base;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("site_info")
public class SiteInfo {

    @TableId(type = IdType.INPUT)
    private Integer id;

    private String homePageTitle;

    private String homePageHref;

    private String logoTitle;

    private String logoImage;

    private String logoHref;

    private String sitename;

    private String domain;

    private int cacheTime;

    private int maxFileUpload;

    private String fileUploadExt;

    private String metaKeywords;

    private String metaDescription;

    private String copyright;
}
