package com.jerry.boot.business.base;

import com.jerry.boot.business.base.pojo.*;
import com.jerry.boot.business.menu.MenuService;
import com.jerry.boot.core.base.validation.Update;
import com.jerry.boot.core.mvc.JsonResult;
import com.jerry.boot.core.mvc.NativeResponse;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/api/base/")
public class BaseController {

    @Resource
    private SiteInfoService siteInfoService;

    @Resource
    private MenuService menuService;

    @GetMapping("/init")
    @NativeResponse
    public InitResp init() {
        InitResp initResp = new InitResp();
        SiteInfo siteInfo = siteInfoService.getSiteInfo();
        HomeInfo homeInfo = new HomeInfo();
        homeInfo.setHref(siteInfo.getHomePageHref());
        homeInfo.setTitle(siteInfo.getHomePageTitle());
        initResp.setHomeInfo(homeInfo);
        LogoInfo logoInfo = new LogoInfo();
        logoInfo.setHref(siteInfo.getLogoHref());
        logoInfo.setTitle(siteInfo.getLogoTitle());
        logoInfo.setImage(siteInfo.getLogoImage());
        initResp.setLogoInfo(logoInfo);
        List<MenuInfo> menuTree = menuService.listMenuTree();
        initResp.setMenuInfo(menuTree);
        return initResp;
    }

    @GetMapping("/siteinfo")
    public SiteInfoDto siteInfo() {
        SiteInfo siteInfo = siteInfoService.getSiteInfo();
        return SiteInfoDto.map(siteInfo);
    }

    @PostMapping("/update-siteinfo")
    public JsonResult updateSiteinfo(@RequestBody @Validated(Update.class) SiteInfoDto siteInfoDto) {
        siteInfoService.updateSiteinfo(siteInfoDto);
        return JsonResult.ok();
    }
}
