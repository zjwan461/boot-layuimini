package com.jerry.boot.business.base;

import com.jerry.boot.business.base.pojo.SiteInfoDto;

public interface SiteInfoService {

    SiteInfo getSiteInfo();

    void updateSiteinfo(SiteInfoDto siteInfoDto);
}
