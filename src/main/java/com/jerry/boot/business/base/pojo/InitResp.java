package com.jerry.boot.business.base.pojo;

import lombok.Data;

import java.util.List;

@Data
public class InitResp {

    private HomeInfo homeInfo;

    private LogoInfo logoInfo;

    private List<MenuInfo> menuInfo;
}
