package com.jerry.boot.business.base.pojo;

public class LogoInfo {

    /**
     * title : LAYUI MINI
     * image : images/logo.png
     * href :
     */

    private String title;
    private String image;
    private String href;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }
}
