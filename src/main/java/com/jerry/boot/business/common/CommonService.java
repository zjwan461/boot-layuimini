package com.jerry.boot.business.common;

import com.jerry.boot.business.file.pojo.FileDto;
import com.jerry.boot.business.user.User;

public interface CommonService {

    User getLoginUser();

    void copyFile2AvatarFile(FileDto fileDto);
}
