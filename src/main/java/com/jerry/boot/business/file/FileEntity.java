package com.jerry.boot.business.file;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName("file")
public class FileEntity {

    @TableId(type = IdType.ASSIGN_ID)
    private Long fileId;

    private String fileName;

    private String originalFileName;

    private String fileType;

    private String filePath;

    private String url;

    private Long fileSize;

    private Long createUserId;

    private Date createTime;

}
