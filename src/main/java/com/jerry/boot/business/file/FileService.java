package com.jerry.boot.business.file;

import com.jerry.boot.business.file.pojo.FileDto;
import org.springframework.web.multipart.MultipartFile;

public interface FileService {

    FileDto upload(MultipartFile file);

    FileDto file(Long fileId);
}
