package com.jerry.boot.business.file.pojo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class FileDto implements Serializable {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long fileId;

    private String fileName;

    private String originalFileName;

    private String fileType;

    @JsonIgnore
    private String filePath;

    private String url;

    private Long fileSize;

    private Long createUserId;

    private Date createTime;

}
