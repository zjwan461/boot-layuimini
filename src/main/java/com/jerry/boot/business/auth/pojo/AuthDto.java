package com.jerry.boot.business.auth.pojo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class AuthDto implements Serializable {

    private String accessToken;

    private String refreshToken;

    private Date refreshTokenTimeOut;

    private String username;
}
