package com.jerry.boot.business.auth.pojo;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class LoginReq {

    @NotBlank(message = "501")
    private String username;
    @NotBlank(message = "501")
    private String password;
    @NotBlank(message = "501")
    private String yzm;
    private boolean rememberMe;
}
