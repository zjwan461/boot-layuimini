package com.jerry.boot.business.auth;

import com.jerry.boot.core.base.ApiException;

import javax.servlet.http.HttpServletRequest;

public interface AuthValidator {

    void validate(HttpServletRequest request) throws ApiException;
}
