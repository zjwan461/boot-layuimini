package com.jerry.boot.business.auth;

import com.jerry.boot.business.auth.pojo.AuthDto;
import com.jerry.boot.business.auth.pojo.LoginReq;
import com.jerry.boot.business.auth.pojo.ReLoginDto;

public interface AuthService {

    AuthDto login(LoginReq loginReq);

    void logout();

    String getYzm();

    AuthDto reLogin(ReLoginDto reLoginDto);
}
