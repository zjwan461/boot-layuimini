package com.jerry.boot.business.auth;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jerry.boot.core.base.ApiException;
import com.jerry.boot.core.base.JConfigureProperties;
import com.jerry.boot.core.web.AbstractJFilter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Component
@Slf4j
public class AuthJFilter extends AbstractJFilter {

    @Resource
    private List<AuthValidator> authValidators;

    protected AuthJFilter(MessageSource messageSource, ObjectMapper objectMapper, JConfigureProperties jConfigureProperties) {
        super(messageSource, objectMapper, jConfigureProperties);
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ApiException {
        for (AuthValidator validator : authValidators) {
            if (log.isDebugEnabled()) {
                log.debug("url:{} auth validator:{} start to validate", request.getServletPath(), validator.getClass().getName());
            }
            validator.validate(request);
            if (log.isDebugEnabled()) {
                log.debug("url:{} auth validator:{} finish to validate", request.getServletPath(), validator.getClass().getName());
            }
        }
    }
}
