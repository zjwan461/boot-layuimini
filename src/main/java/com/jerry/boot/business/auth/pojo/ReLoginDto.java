package com.jerry.boot.business.auth.pojo;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class ReLoginDto {

    @NotBlank(message = "501")
    private String accessToken;

    @NotBlank(message = "501")
    private String refreshToken;


}
