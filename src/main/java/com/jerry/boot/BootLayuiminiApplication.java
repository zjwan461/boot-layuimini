package com.jerry.boot;

import cn.hutool.extra.spring.EnableSpringUtil;
import com.jerry.boot.core.base.JConfigureProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableSpringUtil
@EnableConfigurationProperties(JConfigureProperties.class)
//@MapperScan(basePackages = {"com.jerry.boot.business"})
@EnableTransactionManagement(proxyTargetClass = true)
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class BootLayuiminiApplication {

    public static void main(String[] args) {
        SpringApplication.run(BootLayuiminiApplication.class, args);
    }

}
