package com.jerry.boot.core.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface JFilter {

    boolean deal(HttpServletRequest request, HttpServletResponse response) ;

}
