package com.jerry.boot.core;

public interface JConstant {

    String AUTH_USER_KEY = "_USER";

    String ACCESS_TOKEN = "Access-Token";

    String CHL_HEADER_KEY = "chl";
}
