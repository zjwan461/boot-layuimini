package com.jerry.boot.core.mvc;

import com.jerry.boot.core.base.validation.Select;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class PageReq {

    @NotNull(groups = Select.class)
    private int page;

    @NotNull(groups = Select.class)
    private int limit;
}
