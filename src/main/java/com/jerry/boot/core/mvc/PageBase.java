package com.jerry.boot.core.mvc;

import lombok.Data;

import java.util.List;

@Data
public class PageBase<T> {

    private int code;

    private String msg;

    private List<T> data;

    private Long count;
}
