package com.jerry.boot.core.mvc;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JsonResult {

    private int code;

    private String msg;

    private Object data;

    private String devMsg;

    public static JsonResult ok() {
        return new JsonResult(0, "ok", null, null);
    }

    public static JsonResult ok(Object data) {
        return new JsonResult(0, "ok", data, null);
    }

    public static JsonResult error(int code) {
        return new JsonResult(code, null, null, null);
    }

    public static JsonResult create() {
        return new JsonResult();
    }


    private JsonResult() {
    }

    private JsonResult(int code, String msg, Object data, String devMsg) {
        this.code = code;
        this.msg = msg;
        this.data = data;
        this.devMsg = devMsg;
    }
}
