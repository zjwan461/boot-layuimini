package com.jerry.boot.core.base;

import lombok.Getter;

@Getter
public class ApiException extends SystemException {

    private final int code;

    private String[] args;

    public ApiException(int code) {
        super(code + "");
        this.code = code;
    }

    public ApiException(int code, String... args) {
        super(code + "," + args);
        this.code = code;
        this.args = args;
    }
}
