package com.jerry.boot.core.base;

import javax.servlet.http.HttpServletRequest;

public interface ChannelChecker {

    void check(HttpServletRequest request) throws ChannelCheckException;
}
