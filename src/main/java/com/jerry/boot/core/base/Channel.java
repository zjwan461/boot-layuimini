package com.jerry.boot.core.base;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;

@Slf4j
public enum Channel implements ChannelChecker {

    WEB,
    APP;

    @Override
    public void check(HttpServletRequest request) throws ChannelCheckException {
        if (this.name().equals("WEB")) {
            //todo 校验UA
        } else if (this.name().equals("APP")) {
            //todo 校验APP版本
        } else {
            log.error("unsupported channel {}", this.name());
            throw new ChannelCheckException(this.name());
        }
    }

}
