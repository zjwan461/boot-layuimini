package com.jerry.boot.core.base;

public class ForbiddenException extends ApiException {
    public ForbiddenException() {
        super(403);
    }
}
