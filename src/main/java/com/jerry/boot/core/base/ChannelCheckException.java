package com.jerry.boot.core.base;

public class ChannelCheckException extends ApiException {
    public ChannelCheckException(String channel) {
        super(402, channel);
    }
}
