package com.jerry.boot.core.base;

public class UnauthorizedException extends ApiException {
    public UnauthorizedException() {
        super(401);
    }
}
