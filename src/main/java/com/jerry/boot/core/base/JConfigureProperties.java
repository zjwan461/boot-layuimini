package com.jerry.boot.core.base;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.time.Duration;


@ConfigurationProperties(prefix = "jb")
@Getter
@Setter
public class JConfigureProperties {

    private String rsaPublicKey;

    private String rsaPrivateKey;

    private Duration tokenSaveTime;

    private Duration refreshTokenSaveTime;

    private String loginUrl;

    private String excludeUrls;

    private boolean debug;

    private String uploadPath;

    private String avatarPath;

}
