$ = layui.jquery,
//全局的ajax访问，处理ajax清求时异常
    $.ajaxSetup({
        async: true,
        contentType: "application/json;charset=utf-8",
        beforeSend: function (xhr) {
            let accessToken = localStorage.getItem('accessToken');
            if (accessToken) {
                xhr.setRequestHeader('Access-Token', accessToken);
            }
            // xhr.setRequestHeader('Content-Type', 'application/json;charset=utf-8');
            xhr.setRequestHeader('chl', 'WEB');
        },
        success: function (res, status, xhr) {

        },
        complete: function (xhr,status) {
            layui.layer.closeAll('loading');
        },
        error: function (xhr,status,error) {
            layui.layer.msg('未知异常',{icon: 5})
        },
    });

function beforeResponse(res) {
    console.log(res);
    try {
        if (res.code === 401) {
            var url = window.location.pathname;
            //token不合法、超时且当前不在登录页就处理 ，指定要跳转的页面(比如登陆页). 在此之前先执行重登陆
            if (url.indexOf('page/login-3.html') <= 0) {
                let refreshToken = localStorage.getItem("refreshToken");
                let accessToken = localStorage.getItem("accessToken");
                if (refreshToken || accessToken) {
                    let reqJson = JSON.stringify({refreshToken: refreshToken, accessToken: accessToken});
                    $.post("/api/auth/relogin", reqJson, function (res) {
                        console.log(res);
                        if (res.code === 0) {
                            localStorage.setItem("accessToken", res.data.accessToken);
                            localStorage.setItem("refreshToken", res.data.refreshToken);
                            sessionStorage.setItem("username", res.data.username);
                            window.location.href = url;
                        } else {
                            cleanAndToLogin();
                        }
                    });
                } else {
                    cleanAndToLogin();
                }

            }
        } else if (res.code === 403) {
            //没权限处理
            layui.layer.msg('您没有足够权限处理此请求', {icon: 5})
        } else {
            //其他情况就不统一处理了
        }
    } catch (e) {
    }
}

function cleanAndToLogin() {
    localStorage.removeItem("accessToken");
    localStorage.removeItem("refreshToken");
    window.location.replace("/page/login-3.html");
}

/**
 * 检查token，用于发送请求之前的检查，如果没有保存的token就不call api
 * 减少调用数量
 */
function checkToken() {
    let token = localStorage.getItem("accessToken");
    if (!token) {
        window.location.replace("/page/login-3.html");
    }
}

//获取数据
function get(url, success, callback) {
    checkToken();
    const loading = layui.layer.load(0, {shade: false, time: 2 * 1000});
    $.get(url, function (res) {
        layui.layer.close(loading);
        //只处理正常的情况
        if (res.code === 0) {
            success(res.data)
        } else {
            if (callback) {
                callback(res);
            } else {
                layui.layer.msg('请求失败,msg=' + res.msg, {icon: 5})
            }
        }
    });
}

function post(url, data, success, callback) {
    checkToken();
    const loading = layui.layer.load(0, {shade: false, time: 2 * 1000});
    let reqStr = JSON.stringify(data);
    $.post(url, reqStr, function (res) {
        layui.layer.close(loading);
        //只处理正常的情况
        if (res.code === 0) {
            success(res.data);
        } else {
            if (callback) {
                callback(res);
            } else {
                layui.layer.msg('请求失败,msg=' + res.msg, {icon: 5})
            }
        }
    });
}

function postWithoutCheck(url, data, success, callback) {
    // console.log(url, data);
    const loading = layui.layer.load(0, {shade: false, time: 2 * 1000});
    let reqStr = JSON.stringify(data);
    $.post(url, reqStr, function (res) {
        layui.layer.close(loading);
        //只处理正常的情况
        if (res.code === 0) {
            success(res.data);
        } else {
            callback(res)
        }
    });
}