package com.jerry.boot;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.asymmetric.RSA;
import cn.hutool.json.JSONUtil;
import com.jerry.boot.business.base.BaseController;
import com.jerry.boot.business.base.SiteInfo;
import com.jerry.boot.business.base.SiteInfoService;
import com.jerry.boot.business.base.pojo.MenuInfo;
import com.jerry.boot.business.menu.MenuServiceImpl;
import com.jerry.boot.business.user.User;
import com.jerry.boot.business.user.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.MessageSource;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@SpringBootTest
class BootLayuiminiApplicationTests {

    @Resource
    private MessageSource messageSource;

    @Resource
    private UserMapper userMapper;

    @Resource
    private SiteInfoService siteInfoService;

    @Resource
    private MenuServiceImpl menuService;

    @Resource
    private BaseController baseController;

    @Test
    public void addUser() {
        User user = new User();
        user.setGender(1);
        String salt = IdUtil.nanoId();
        user.setSalt(salt);
        user.setUsername("admin");
        user.setPassword(SecureUtil.md5(user.getSalt() + "admin"));
        user.setNextChangePasswordTime(DateUtil.offsetDay(new Date(), 30));
        userMapper.insert(user);
    }

    @Test
    public void test1() {

        String message = messageSource.getMessage("401", null, Locale.CHINA);
        System.out.println(message);
    }

    @Test
    public void test2() {
        RSA rsa = new RSA();
        System.out.println(rsa.getPublicKeyBase64());
        System.out.println(rsa.getPrivateKeyBase64());
    }

    @Test
    public void test3() {
        SiteInfo siteInfo = siteInfoService.getSiteInfo();
        System.out.println(siteInfo);
    }

    @Test
    public void test4() {
//        List<MenuInfo> list = menuService.list(Wrappers.lambdaQuery(MenuInfo.class)
//                .eq((SFunction<MenuInfo, Integer>) MenuInfo::getMenuType, 0));
//        list.forEach(System.out::println);
        List<MenuInfo> menuInfos = menuService.listMenuTree();
        System.out.println(JSONUtil.toJsonPrettyStr(menuInfos));
    }

    @Test
    public void test5() {
        System.out.println(JSONUtil.toJsonPrettyStr(baseController.init()));
    }
}
